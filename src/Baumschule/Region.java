package Baumschule;

import java.util.List;

public class Region
{
	private List<Tree> trees;
	private String name;
	private int size;	
	
	public Region(List<Tree> trees, String name, int size)
	{
		super();
		this.trees = trees;
		this.name = name;
		this.size = size;
	}

	public void addTree(Tree tree)
	{
		this.trees.add(tree);
	}
	
}
