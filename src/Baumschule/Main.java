package Baumschule;

import java.util.ArrayList;
import java.util.List;

public class Main
{
	public static void main(String[] args)
	{	
		List<Tree> trees = new ArrayList();
		Region r1 = new Region(trees, "Region 1", 20);
		Fertilisation tg = new TopGreen();
		Fertilisation sg = new SuperGrow();
		Tree con1 = new Conifer(25, 5, sg);
		Tree broad1 = new Broadleaf(29, 10, tg);
		
		trees.add(con1);
		trees.add(broad1);	

		for(Tree tree : trees)
		{
			System.out.println(tree.getFertilisation());
		}
	}
}
