package Baumschule;

public abstract class Tree
{
	public Tree(int maxSize, int maxDiameter, Fertilisation fertilisation)
	{
		super();
		this.maxSize = maxSize;
		this.maxDiameter = maxDiameter;
		this.fertilisation = fertilisation;
	}
	private int maxSize;
	private int maxDiameter;
	private Fertilisation fertilisation;
	
	public Fertilisation getFertilisation()
	{
		return fertilisation;
	}
}
