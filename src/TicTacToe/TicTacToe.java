package TicTacToe;

import java.util.Scanner;

public class TicTacToe
{
	private int[][] grid = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
	private Scanner scan = new Scanner(System.in);

	public void selection()
	{
		int row = getInput("In which row from 1 to 3 do you want to place your tick?");
		int colum = getInput("In which colum from 1 to 3 do you want to place your tick?");
		
		grid[row][colum] = 1;
	}

	private int getInput(String input)
	{
		int tempInput;
		boolean falseInput;
		
		do
		{
		System.out.println(input);
		tempInput = scan.nextInt();
		if (tempInput >= 1 && tempInput <= 3)
		{
			System.out.println("Your choice: " + tempInput);
			falseInput = false;
		}
		else
		{
			falseInput = true;
			System.out.println("You can only choose a number between 1 and 3. Please enter your choice again.");
		}
		}
		while(falseInput == true);
		
		return tempInput;
	}
	
	
	public void print()
	{
		for(int i = 0; i < grid.length;i++)
		{
			for(int j = 0; j < grid[i].length;j++)
			{
				if( j <= 1)
				{
					System.out.print(grid[i][j]);
				}
				else
				{
					System.out.println(grid[i][j]);
				}
			}
		}
	}
	
	
}
