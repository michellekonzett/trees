package ObserverPattern;

import java.util.List;

public class Sensor implements Observer
{

	private List <Observable> observables;
	
	
	@Override
	public void addItem(Observable observable)
	{
		this.observables.add(observable);
	}

	@Override
	public void informAll(String info)
	{
		for(Observable obs : observables)
		{
			obs.inform(info);
		}
		
	}
}
