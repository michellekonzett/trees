package ObserverPattern;

import java.util.List;

public interface Observer
{
	void addItem(Observable obervable);
	void informAll(String info);
}
