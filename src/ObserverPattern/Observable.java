package ObserverPattern;

public interface Observable
{
	void inform(String info);
}
