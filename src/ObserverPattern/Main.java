package ObserverPattern;

public class Main
{
	public static void main(String[] args) throws InterruptedException {
		Observer sensor = new Sensor();
		
		sensor.addItem(new Lantern());
		sensor.addItem(new Christmastree());

		
		sensor.informAll("Lights out");
		
		Thread.sleep(1000);
		
		System.out.println();
		sensor.informAll("Lights on");
	}
}
