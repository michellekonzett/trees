package Player;

import java.util.ArrayList;
import java.util.List;

public class CD implements Playable
{
	String name;
	
	List<Song> songs;
	
	public String getName()
	{
		return name;
	}
	
	public void play()
	{
		
	}

	public void addSong(Song song)
	{
		songs.add(song);
	}

	public CD(String name)
	{
		super();
		this.name = name;
		songs = new ArrayList<Song>();
	}
}
