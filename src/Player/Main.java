package Player;

public class Main
{
	public static void main(String[] args)
	{
		Actor a1 = new Actor("Katy","Perry");
		
		Song s1 = new Song("Hey", 120);
		Song s2 = new Song("Bye", 60);
		
		CD cd1 = new CD("That Mix");
		
		Player p1 = new Player();
		
		
		a1.addRecord(cd1);
		cd1.addSong(s1);
		
		p1.playSong(s1);
		
		
		DVD dvd1 = new DVD("That Film");
		
		dvd1.addTitle(new Title("Chapter 1", 950));
	}
}
