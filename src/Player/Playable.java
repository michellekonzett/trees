package Player;

public interface Playable
{
	String name;
	int length;
	
	public String getName()
	{
		return name;
	}
	
	public void play();
}
