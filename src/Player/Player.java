package Player;

public class Player
{
	private String state;
	
	public Player()
	{
		super();
		this.state = "stopped";
	}
	
	public void playSong(Playable playable)
	{
		state = "playing";
		playable.play();
	};
	
	public void stop()
	{
		state = "stopped";
	};
	
	public void pause()
	{
		state = "paused";
	};	
}
