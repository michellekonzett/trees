package Bankomat;

import java.util.Scanner;

public class Main
{
	public static void main(String[] args)
	{
		double Kontostand = 0;
		double H�he = 0; //der Ein-/Auszahlung
		int Auswahl = 0; //der Aktion im "Men�"
		boolean running = true;
		
		Scanner scanner = new Scanner(System.in);
		String input;
		
		
		
		while(running == true)
		{
			if(Auswahl == 1)
			{	
				System.out.println("Einzuzahlender Betrag: ");
				input = scanner.nextLine();
				H�he = Float.parseFloat(input);
				
				Kontostand+=H�he;
			}
			else if(Auswahl == 2)
			{
				System.out.println("Auszuzahlender Betrag: ");
				input = scanner.nextLine();
				H�he = Float.parseFloat(input);
				
				Kontostand-=H�he;
			}
			else if(Auswahl == 3)
			{
				System.out.println("Aktueller Kontostand: " + Kontostand);
			}
			else if(Auswahl == 4)
			{
				running = false;
			}
			else if(Auswahl > 4 || Auswahl < 0)
			{
				System.out.println("Ung�ltige Eingabe");
			}
			
		}

	}
}
