package Sortingmethods;

public class SelectionSort implements SortingMethod
{
	@Override
	public int[] doSort(int[] unsorted)
	{
		int[] sorted = unsorted;
		
		for(int i = 0; i < sorted.length - 1; i++)
		{
			int smallest = i;
			for(int j = i+1; j < sorted.length; j++)
			{
				if(sorted[j] < sorted[smallest])
				{
					smallest = j;
				}
			}

			int temp = sorted[i];
			sorted[i] = sorted[smallest];
			sorted[smallest] = temp;
		}		
		return sorted;
	}
}
