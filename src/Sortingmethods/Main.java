package Sortingmethods;

public class Main
{
	public static void main(String[] args)
	{
		SortingMethod bubble = new BubbleSort();
		SortingMethod insertion = new InsertionSort();
		SortingMethod selection = new SelectionSort();
		
		int unsorted[] = {9,12,5,8,3};
		
		//Bubble Sort
		int[] sorted = bubble.doSort(unsorted);
		String out = "";
		out += out + sorted[0];
		for(int i = 1; i< sorted.length; i++)
		{
			out += ", " + sorted[i];
		}
		System.out.println(out);
		
		//Insertion Sort
		sorted = insertion.doSort(unsorted);
		out = "";
		
		out += out + sorted[0];
		for(int i = 1; i< sorted.length; i++)
		{
			out += ", " + sorted[i];
		}
		System.out.println(out);
		
		//Selection Sort
		sorted = selection.doSort(unsorted);
		out = "";
		
		out += out + sorted[0];
		for(int i = 1; i< sorted.length; i++)
		{
			out += ", " + sorted[i];
		}
		System.out.println(out);
	}
}
