package Sortingmethods;

public class InsertionSort implements SortingMethod
{

	@Override
	public int[] doSort(int[] unsorted)
	{
		int[] sorted = unsorted;
		
		int i = 1;
		int j = 0;
		
		while (i < sorted.length)
		{
			j = i;
			
			while (j > 0 && sorted[i-1] > sorted[i])
			{
				int temp = sorted[i];
				sorted[i] = sorted[i-1];
				sorted[i-1] = temp;
				
				j = j-1;
			}
			i = i + 1;
		}
		
		return sorted;
	}
}
