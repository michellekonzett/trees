package Sortingmethods;

public interface SortingMethod
{
	int [] doSort(int[] unsorted);
}
