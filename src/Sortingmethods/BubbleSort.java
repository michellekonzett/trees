package Sortingmethods;

public class BubbleSort implements SortingMethod
{

	@Override
	public int[] doSort(int[] unsorted)
	{
		int[] sorted = unsorted;
		int n = sorted.length;
		boolean swapped = true;
		
		do 
		{
			swapped = false;
			for(int i = 1; i < n; i++)
			{
				if( sorted[i-1] > sorted[i])
				{
					int temp = sorted[i];
					sorted[i] = sorted[i-1];
					sorted[i-1] = temp;
					swapped = true;
				}
			}
		} while (swapped == true);
		
		return sorted;
	}
}
